<?php include 'header.php'; ?>
	
	<div class="construction-single">

		<?php
			
			$header_image = get_field('header_image');
			$header_image = $header_image[sizes][header_bgd];
			$header_title = get_field('header_title');
			$header_tag   = get_field('header_tag');
		
		?>
		
		<div class="page-header" style="background-image: url(<?php echo $header_image ?>); ">
			
			<?php
			
				if ($header_title) : echo '<h1>', $header_title, '</h1>'; endif;
				if ($header_tag) : echo $header_tag; endif;	
				
			?>
		
		</div><!-- /.page-header ends -->		

		<div class="page-opening">
						
			<div class="inner-text">	

			<?php 
		
				$opening_title = get_field('opening_title');
				$opening_text  = get_field('opening_text');

				if ($opening_title) : echo '<h2>', $opening_title, '</h2>'; endif;
				if ($opening_text) : echo $opening_text; endif;
			
			?>

			</div><!-- /.inner-text ends -->
			
		</div><!-- /.page-opening ends -->				
		
		<div class="section-one page-split">
			
			<?php
				
				$first_split_image = get_field('first_split_image');
				$first_split_title = get_field('first_split_title');
				$first_split_text  = get_field('first_split_text');
			
			?>
			
			<div class="first makeSiblingHeight responsive-big" style="background-image: url(<?php echo $first_split_image; ?>);"></div><!--/.first ends -->
			<div class="responsive-small"><img src="<?php echo $first_split_image; ?>"/></div>
			
			<div class="second sibling">

				<div class="split-text">

					<?php
						
						if($first_split_title) : echo '<h3>', $first_split_title, '</h3>'; endif;
						if($first_split_text) : echo $first_split_text; endif;
						
					?>

				</div><!--/.split-text ends -->
			
			</div><!--/.second ends -->
			
		</div><!-- /.section-one ends -->


		<div class="section-two">
			<?php
				$repeater = 'why_choose_item';
				$why_choose_title = get_field('why_choose_title');
				
				if($why_choose_title) : echo "<h3 class='mB'>", $why_choose_title, "</h3>"; endif;	
			?>
			
			<div class="features-wrapper">
				
			<?php 
				
				if( have_rows($repeater) ) : 
					
				while( have_rows($repeater) ) : the_row();
				
					$repeater_icon  = get_sub_field('icon');
					$repeater_title = get_sub_field('title');
					$repeater_text  = get_sub_field('text');
							
			?>
				
				<div class="feature">
					
					<div class="icon">
						<?php if($repeater_icon) : ?>
							<img src="<?php echo $repeater_icon[sizes][thumbnail]; ?>" alt="<?php echo $repeater_icon[title]; ?>"/>
						<?php endif; ?>
					</div><!-- /.icon ends -->
					
					<div class="meta">
						<?php if($repeater_title) : echo '<h3>', $repeater_title, '</h3>'; endif; ?>
						<?php if($repeater_text) : echo '<p>', $repeater_text, '</p>'; endif; ?>
					</div><!-- /.meta ends -->
				
				</div><!-- /.feature ends -->

				<?php endwhile; ?>
				
				<?php endif; ?>
				
			</div><!-- /.features-wrapper ends -->

		</div><!-- /.section-two ends -->
		
		
		<div class="section-three page-split">

			<?php
				
				$second_split_image = get_field('second_split_image');
				$second_split_title = get_field('second_split_title');
				$second_split_text  = get_field('second_split_text');
			
			?>

			<div class="first makeSiblingHeight responsive-big" style="background-image: url(<?php echo $second_split_image; ?>);"></div><!--/.first ends -->
			<div class="responsive-small"><img src="<?php echo $second_split_image; ?>"/></div>
			
			<div class="second sibling">

				<div class="split-text">

					<?php
						
						if($second_split_title) : echo '<h3>', $second_split_title, '</h3>'; endif;
						if($second_split_text) : echo $second_split_text; endif;
						
					?>

				</div><!--/.split-text ends -->
			
			</div><!--/.second ends -->
	
		</div><!-- /.section-three ends-->
		
		
		<div class="section-four">

			<?php
				
				$fullwidth_image = get_field('full_width_image');
				$fullwidth_image = $fullwidth_image[sizes][full_width_split];
				$fullwidth_title = get_field('full_width_title');
				$fullwidth_text  = get_field('full_width_text');
			
			?>
			
			<img src="<?php echo $fullwidth_image; ?>" alt="home-cinema-section4-image"/>
		
			<div class="meta">
				<?php if($fullwidth_title) : echo '<h3>', $fullwidth_title, '</h3>'; endif; ?>
				
				<?php if($fullwidth_text) : ?>
				<div class="text">
					<?php echo $fullwidth_text; ?>
				</div><!--/.text ends -->
				<?php endif; ?>
			</div>
		
		</div><!-- /.section-four ends -->

		
<!--
		<div class="section-five page-split">

			<?php
				
				$third_split_image = get_field('third_split_image');
				$third_split_title = get_field('third_split_title');
				$third_split_text  = get_field('third_split_text');
			
			?>
			
			<div class="first makeSiblingHeight" style="background-image: url(<?php echo $third_split_image; ?>);">
							
			</div>
			
			<div class="second sibling">

				<div class="split-text">
					
					<?php
						
						if($third_split_title) : echo '<h3>', $third_split_title, '</h3>'; endif;
						if($third_split_text) : echo $third_split_text; endif;
						
					?>
								
				</div>
			
			</div>
			
		</div>
-->
		
		
		<div class="section-six page-split">

			<?php
				
				$fourth_split_image = get_field('fourth_split_image');
				$fourth_split_title = get_field('fourth_split_title');
				$fourth_split_text  = get_field('fourth_split_text');
			
			?>

			<div class="first makeSiblingHeight responsive-big" style="background-image: url(<?php echo $fourth_split_image; ?>);"></div><!--/.first ends -->
			<div class="responsive-small"><img src="<?php echo $fourth_split_image; ?>"/></div>
			
			<div class="second sibling">

				<div class="split-text">
					
					<?php
						
						if($fourth_split_title) : echo '<h3>', $fourth_split_title, '</h3>'; endif;
						if($fourth_split_text) : echo $fourth_split_text; endif;
						
					?>
					
				</div><!--/.split-text ends -->
			
			</div><!--/.second ends -->
	
		</div><!-- /.section-six ends-->
		
	</div><!--/.construction-single ends -->
		
<?php include 'footer.php'; ?>