<?php get_header(); ?>

	<div class="index-page">
		
		<?php
			
			$header_image = get_field('header_image');
			$header_image = $header_image[sizes][header_bgd];
			$header_title = get_field('header_title');
			$header_tag   = get_field('header_tag');
		
		?>
		<div class="page-header" style="background-image: url(<?php echo $header_image ?>); ">
			
			<?php
			
			if ($header_title) : echo '<h1>', $header_title, '</h1>'; endif;
			if ($header_tag) : echo $header_tag; endif;	
				
			?>
		
		</div><!-- /.page-header ends -->		
			
		
		<div class="page-opening">
						
			<div class="inner-text">	

			<?php 
		
				$opening_title = get_field('opening_title');
				$opening_text  = get_field('opening_text');

				if ($opening_title) : echo '<h2>', $opening_title, '</h2>'; endif;
				if ($opening_text) : echo $opening_text; endif;
			
			?>

			</div><!-- /.inner-text ends -->
			
		</div><!-- /.page-opening ends -->
		
		
		<div class="content">
			
			<div class="inner-text">
			
				<?php 
				
					if ( have_posts() ) : while ( have_posts() ) : the_post();				
				
						the_content();
						
					endwhile;
					endif;
				?>
						
			</div><!-- /.inner-text ends -->
			
		</div><!-- /.content ends -->	
						
	</div><!-- /.index-page ends -->
				
<?php get_footer(); ?>