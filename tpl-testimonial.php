<?php 

/**
 * Template Name: Testimonial
 *
 */

get_header();
	
	$args = array(
		'post_type'      => 's_testimonial',
		'posts_per_page' => -1,
		//'orderby'        => 'menu_order',
		//'order'          => 'ASC',
	);

	$testimonial_items = new WP_Query( $args );

?>

	<div class="testimonial-page">
		
		<?php
			
			$header_image = get_field('header_image');
			$header_image = $header_image[sizes][header_bgd];
			$header_title = get_field('header_title');
			$header_tag   = get_field('header_tag');
		
		?>
		<div class="page-header" style="background-image: url(<?php echo $header_image ?>); ">
			
			<?php
			
			if ($header_title) : echo '<h1>', $header_title, '</h1>'; endif;
			if ($header_tag) : echo $header_tag; endif;	
				
			?>
		
		</div><!-- /.page-header ends -->		
			
		
		<div class="page-opening">
						
			<div class="inner-text">	

			<?php 
		
				$opening_title = get_field('opening_title');
				$opening_text  = get_field('opening_text');

				if ($opening_title) : echo '<h2>', $opening_title, '</h2>'; endif;
				if ($opening_text) : echo $opening_text; endif;
			
			?>

			</div><!-- /.inner-text ends -->
			
		</div><!-- /.page-opening ends -->
		
				
		<div class="testimonials-wrapper">
			
			<?php if($testimonial_items->have_posts() ) : 
				
				while ($testimonial_items->have_posts()) : $testimonial_items->the_post(); ?>
				
				<?php
					
					$avatar = get_field('testimonial_avatar');
					$name   = get_field('testimonial_name');
					$sector = get_field('testimonial_sector');
					$text   = get_field('testimonial_text');
				
				?>
				
				<div class="testimonial">
					
					<div class="avatar">
						<img src="<?php echo $avatar; ?>" alt="avatar image" />
					</div><!-- /.avatar ends -->
					
					<div class="meta">
						
						<p class="name"><?php echo $name; ?></p>
					
						<p class="field"><?php echo $sector; ?></p>
					
						<p><?php echo $text; ?></p>
					
					</div><!--/. meta ends -->
					
				</div><!-- /.testimonial ends -->

			
			<?php endwhile; ?>
			
			<?php endif; ?>
			
			<?php wp_reset_postdata(); ?>	
			
		</div><!-- /.testimonials-wrapper -->
				
	</div><!-- /.testimonial ends -->
		
<?php include 'footer.php'; ?>