<?php 

/**
 * Template Name: About
 *
 */

get_header();

?>
	
	<div class="about-page">
		
		<?php
			
			$header_image = get_field('header_image');
			$header_image = $header_image[sizes][header_bgd];
			$header_title = get_field('header_title');
			$header_tag   = get_field('header_tag');
		
		?>
		<div class="page-header" style="background-image: url(<?php echo $header_image ?>); ">
			
			<?php
			
			if ($header_title) : echo '<h1>', $header_title, '</h1>'; endif;
			if ($header_tag) : echo $header_tag; endif;	
				
			?>
		
		</div><!-- /.page-header ends -->		
			
		
		<div class="page-opening">
						
			<div class="inner-text">	

			<?php 
		
				$opening_title = get_field('opening_title');
				$opening_text  = get_field('opening_text');

				if ($opening_title) : echo '<h2>', $opening_title, '</h2>'; endif;
				if ($opening_text) : echo $opening_text; endif;
			
			?>

			</div><!-- /.inner-text ends -->
			
		</div><!-- /.page-opening ends -->
		
		<div class="page-split">
			
			<?php
				
				$upper_split_image = get_field('upper_split_image');
				$upper_split_title = get_field('upper_split_title');
				$upper_split_tag   = get_field('upper_split_tag');
				$upper_split_text  = get_field('upper_split_text');
			
			?>
			
			<div class="first makeSiblingHeight responsive-big" style="background-image: url(<?php echo $upper_split_image; ?>);"></div><!--/.first ends -->
			<div class="responsive-small"><img src="<?php echo $upper_split_image; ?>"/></div>
			
			<div class="second sibling">

				<div class="split-text">
					
					<?php
						
						if($upper_split_title) : echo '<h3>', $upper_split_title, '</h3>'; endif;
						if($upper_split_tag) : echo '<p class=\'split-text-tag\'>', $upper_split_tag, '</p>'; endif;
						if($upper_split_text) : echo $upper_split_text; endif;
						
					?>
					
				</div><!--/.split-text ends -->
			
			</div><!--/.second ends -->
			
		</div><!--/.page-split ends -->
			
		<div class="content">
			
			<div class="inner-text">
			
				<?php 
				
					if ( have_posts() ) : while ( have_posts() ) : the_post();				
				
						the_content();
						
					endwhile;
					endif;
				?>
						
			</div><!-- /.inner-text ends -->
			
		</div><!-- /.content ends -->
		
		<div class="page-split">

			<?php
				
				$lower_split_image = get_field('lower_split_image');
				$lower_split_title = get_field('lower_split_title');
				$lower_split_text  = get_field('lower_split_text');
			
			?>

			<div class="first makeSiblingHeight responsive-big" style="background-image: url(<?php echo $lower_split_image; ?>);"></div><!--/.first ends -->
			<div class="responsive-small"><img src="<?php echo $lower_split_image; ?>"/></div>
			
			<div class="second sibling">

				<div class="split-text">

					<?php
						
						if($lower_split_title) : echo '<h3>', $lower_split_title, '</h3>'; endif;
						if($lower_split_text) : echo $lower_split_text; endif;
						
					?>

				</div><!--/.split-text ends -->
			
			</div><!--/.second ends -->
			
		</div><!--/.page-split ends -->

		
	</div><!-- /.about-page ends -->
		
<?php get_footer(); ?>