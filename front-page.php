<?php get_header(); ?>
	
	<!-- / temp class that will be generated dynamically and added to body class in wordpress -->
	<div class="home-page">
		
		<div class="page-header">
			<h1>Looking To <a href="<?php echo esc_url( home_url( '/home-automation' ) ); ?>">Automate</a> Or <a href="<?php echo esc_url( home_url( '/construction' ) ); ?>">Renovate</a> Your Home?</h1>
			<p>Our award winning team at Smarta Construction & Technology offer customers high quality and affordable home automation and construction services to satisfy any given specification.</p>
			<a class="button" id="tell-me-more-link" href="#tell-me-more-anchor">tell me more about smarta</a>
			<div class="scroll-down-graphic"></div>
		</div><!-- /.page-header ends -->
		
		<div id="tell-me-more-anchor" class="page-opening">
			<div class="inner-text">
		
			<?php 
		
				$opening_title = get_field('opening_title');
				$opening_text  = get_field('opening_text');

				if ($opening_title) : echo '<h2>', $opening_title, '</h2>'; endif;
				if ($opening_text) : echo $opening_text; endif;
			
			?>
						
			</div><!-- /.inner-text ends -->
		</div><!-- /.page-opening ends -->
			
<?php
	
	$args = array(
		'post_type' => array( 's_construction', 's_home_automation' ),
		'posts_per_page' => -1,
		//'orderby'        => 'menu_order',
		//'order'          => 'ASC',
	);
	$featured_posts = new WP_Query( $args );

?>

			
		<div class="ql-wrapper isotope-section">
							
			<nav class="ql-nav isotope-filters">
				<ul>
					<li><a href="#" class="button-join-left selected" data-filter=".home-automation">home automation</a></li>
					<li><a href="#" class="button-join-right" data-filter=".construction">construction</a></li>
				</ul>
			</nav><!-- ql ends -->
				
			<div class="ql-content isotope-grid">
				
				<?php if($featured_posts->have_posts()) :
					
					while ($featured_posts->have_posts()) : $featured_posts->the_post() ?>
						
						<?php 
							$post_type = get_post_type();
							$filter_name = ($post_type === 's_construction') ? 'construction' : 'home-automation';
							$excerpt_text = get_field('excerpt_text');
						?>
				
						<a href="<?php the_permalink(); ?>">
							<div class="ql-item isotope-item <?php echo $filter_name; ?>">
								<h4><?php the_title(); ?></h4>
								<p><?php echo $excerpt_text; ?></p>
								<p class="more">Find out more</p>
							</div>
						</a>
					
					<?php endwhile; ?>
					
				<?php endif; ?>
				
				<?php wp_reset_postdata(); ?>

<!--
				<div class="more-link-wrapper isotope-stamp">
					<a class="button" href="#">find out more</a>
				</div>
-->
				<!-- /.more-link-wrapper ends-->			
				
			</div><!-- ./ql-content ends -->			
			
		</div><!-- /.ql-wrapper ends -->
					
		<div class="featured-section">
			<div class="inner-text">
			
				<?php 
					
					$featured_title = get_field('featured_title');
					$featured_text  = get_field('featured_text');
		
					if ($featured_title) : echo '<h2>', $featured_title, '</h2>'; endif;
					if ($featured_text) : echo $featured_text; endif;
				
				?>
			
			</div><!-- /.inner-text ends -->
		</div><!-- /.featured-section ends -->
		
		
		<div class="slider-section">

			<?php $repeater = 'home_slider';
			
			if( have_rows($repeater) ) :
			
			while( have_rows($repeater) ) : the_row();
			
				$slider_image   = get_sub_field('slider_image');
				$slider_tag     = get_sub_field('slider_tag');
				$slider_title   = get_sub_field('slider_title');
				$slider_caption = get_sub_field('slider_caption');
				$slider_link    = get_sub_field('slider_link');
			
			?>
			
			<div class="single-slide">
				<img src="<?php echo $slider_image; ?>" alt="slider image">
				<div class="slide-meta">
					<p class="slide-tag"><?php echo $slider_tag; ?></p>
					<p class="slide-heading"><?php echo $slider_title; ?></p>
					<p class="slide-caption"><?php echo $slider_caption; ?></p>
					<a class="button slider-button" href="<?php echo $slider_link; ?>">view the project</a>
				</div>
			</div><!-- /.single-slide ends -->
			
			<?php endwhile; ?>
			
			<?php endif; ?>
						
		</div><!-- /.slider-section ends -->
		
	</div><!--/.home-page ends -->	
		
<?php get_footer(); ?>