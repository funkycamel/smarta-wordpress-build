// remap jQuery to $
(function($){


/* trigger when page is ready */
$(document).ready(function (){


	// Jquery custom functions
	
	// Helper to detect if variable exists
	jQuery.fn.exists = function() {
		return this.length>0;
	}

	
	jQuery.fn.makeParentHeight = function() {
	    
	    this.css('height', '');
	    
	    return this.css("height", $(this).parent().height() + 'px' );
	};

	jQuery.fn.makeSiblingHeight = function() {
		this.css('height', '');
		
		return this.css('height', $(this).parent().children('.sibling').height() + 'px');
	}


	// Variables
	
	var $html             = $('html');
	var $body             = $('body');
	var $contentWrapper   = $('.content-wrapper');
	var $fixedOverlay     = $('.fixed-overlay');
	var overlay           = false; // helper to determine if overlay is visible
	var $isotope          = $('.isotope-grid');
	var $homeIsotope      = $('.home-page .isotope-grid');
	var $menuToggle       = $('.menu-btn');
	var $menuClose        = $('.menu-btn-close'); 
	var $siblingHeight    = $('.makeSiblingHeight');
//	var $menuChildren   = $('.main-navigation .menu-item-has-children');
	var $menuChildrenLink = $('.main-navigation .menu-item-has-children > a');
	var $subMenu          = $('.menu-item-has-children > .sub-menu');
	var test              = '';
	var $mainLogo         = $('.header-inner .logo');
	var $tellMeMoreLink   = $('#tell-me-more-link');
//	var $aboutUsSplit   = $('.about-us-split .second');
//	var $menuToggle = $('.menu-toggle');
	//$subMenu.fadeOut();
	
	// Add an animated scroll to specific section on the home page
	$tellMeMoreLink.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 30 + 'px'
        }, 1000);
    });	
	
//	$menuChildren.on('click', function(e) {
		
//		$(this).toggleClass('show-sub');	
		
//		console.log($(this).children('.sub-menu').outerHeight());

		
//		e.preventDefault();
		
		
//		if($(this).hasClass('show-sub')) {
//			$(this).children('.sub-menu').fadeIn(200);
//		} else {
//			$(this).children('.sub-menu').fadeOut(200);
//		}

//	});
	
	// anything that needs to wait for the window to be loaded	
	$(window).load(function(){
			
		if ($isotope.length > 0) {
						
			$isotope.isotope({
				itemSelector: '.isotope-item',
				layoutMode: 'fitRows',
				stamp: '.isotope-stamp'
			});
			
			// set default for home filter
			if ($homeIsotope.length > 0 ) {
				$homeIsotope.isotope({ filter: '.home-automation' });
			}
			
			// Trigger isotope after images have loaded
			$isotope.imagesLoaded().progress( function() {
				$isotope.isotope('layout');
			})
			
			// filter items on link click
			$('.isotope-filters li a').on( 'click', function(e) {
				
				var $this = $(this);
				
				if($this.hasClass('selected')) {
					return false;
				}
				
				var $optionSet = $this.parents('.isotope-filters');
				$optionSet.find('.selected').removeClass('selected');
				$this.addClass('selected');
				
				
				var $filterValue = $this.attr('data-filter');
				$isotope.isotope({ filter: $filterValue });
				
				e.preventDefault();
				
			});
			
		}// isotope ends

		// Adjust heights based on sibling.
		// also caled in resize function	
		$siblingHeight.each(function() {
			$(this).makeSiblingHeight();
		});

	});
	
	// Adjusts height to parent div, call on resize aswell
//	$aboutUsSplit.makeParentHeight();
	
	// Toggle the main nav menu
	$menuToggle.on('click', function() {
		$(this).toggleClass('close');
		$body.toggleClass('push');
		$html.toggleClass('open');
	
		if (overlay === false) {
			overlay = true
		} else {
			overlay = false;
		}
	
	});
	
	// When on smaller devices - toggle menu close
	$menuClose.on('click', function() {
		
		$menuToggle.toggleClass('close');
		$body.toggleClass('push');
		$html.toggleClass('open');

		if (overlay === false) {
			overlay = true
		} else {
			overlay = false;
		}
	
	})

	// Clicking on the overlay makes the nav close if open
	$fixedOverlay.on('click', function() {
				
		if (overlay === true) {
			$body.toggleClass('push');
			$html.toggleClass('open');
			$menuToggle.toggleClass('close');
			
			overlay = false;
		}
	});	
	
	
	// The slide out nav drop down mechanism
	$menuChildrenLink.each(function(index) {
		
		var maxHeight = $(this).parent().children('.sub-menu').outerHeight();
		
		$(this).parent().children('.sub-menu').css('max-height', 0);
		
		$(this).on('click', function(e) {
			
			$(this).parent().toggleClass('show-sub');
			
			e.preventDefault();
			
			if($(this).parent().hasClass('show-sub')) {
				$(this).parent().children('.sub-menu').css('max-height', maxHeight);
				console.log('yes');
			} else {
				$(this).parent().children('.sub-menu').css('max-height', 0);
				console.log('yes');
			}
			
		});
		
	});
	

	
	// Call slider
	var $slider = $('.slider-section').bxSlider({
	
		adaptiveHeight: true,
		auto: true,
		useCSS: false
		
	});
	
	// This will only work on page load not when dragging the screen size
	if($(window).width() > 799) {
	
		if($slider.length > 0) {
			$slider.stopAuto();
		}
		
	}
	
	
	// anything that requires a scroll count
	$(window).scroll(function(){
		
		var $header = $('.main-header');
		
		if ($(this).scrollTop() > 50) {
			$header.addClass('scrolled');
//			$mainLogo.attr('src', 'http://www.jamiepotepa.co.uk/smarta_wordpress/wp-content/themes/smarta/_/images/smarta_logo_reversed.png');
			$mainLogo.addClass('logo-reversed');
		}
		
		if ($(this).scrollTop() < 50 && $header.hasClass('scrolled')) {
			$header.removeClass('scrolled');
//			$mainLogo.attr('src', 'http://www.jamiepotepa.co.uk/smarta_wordpress/wp-content/themes/smarta/_/images/smarta_logo.png');
			
			if($mainLogo.hasClass('logo-reversed')) {
				$mainLogo.removeClass('logo-reversed');
			}
		}
		
	});
	
	$(window).resize(function() {
	
		//$aboutUsSplit.makeParentHeight();

		$siblingHeight.each(function() {
			$(this).makeSiblingHeight();
		});

	
	});
	

});
	
	


/* optional triggers

$(window).load(function() {
	
});

$(window).resize(function() {
	
});

*/


})(window.jQuery);