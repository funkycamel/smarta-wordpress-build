<?php

/*
	*	-----------------------------------------------------------------
	*	This file creates and contains the testimonial post type
	*	-----------------------------------------------------------------
*/

	// Create s_testimonial custom post type
	
	function testimonial_custom_post() {
		
		$labels = array (
			'name'               => 'Testimonial',
			'singular_name'      => 'Testimonial',
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New Testimonial Post',
			'edit_item'          => 'Edit Testimonial Post',
			'new_item'           => 'New Testimonial Post',
			'all_items'          => 'All Testimonial Posts',
			'view_item'          => 'View Testimonial Post',
			'search_items'       => 'Search Testimonial Posts',
			'not_found'          => 'No Testimonial Posts Found',
			'not_found_in_trash' => 'No Testimonial Posts Found In Trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Testimonial',
		);
		
		$args = array (
			'labels'        => $labels,
			'public'        => true,
			'rewrite'       => array( 'slug' => 'Testimonial' ),
//			'menu_position' => 20,	
		);

		register_post_type ( 's_testimonial', $args );		
	}			
	
	add_action( 'init', 'testimonial_custom_post' );	

	// flush for permalinks
	function my_rewrite_flush_testimonial() {
		flush_rewrite_rules();
	}
	add_action( 'after_switch_theme', 'my_rewrite_flush_testimonial' );
?>