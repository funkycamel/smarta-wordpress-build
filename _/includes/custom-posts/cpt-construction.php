<?php

/*
	*	-----------------------------------------------------------------
	*	This file creates and contains the construction post type
	*	-----------------------------------------------------------------
*/

	// Create s_construction custom post type
	
	function construction_custom_post() {
		
		$labels = array (
			'name'               => 'Construction',
			'singular_name'      => 'Construction',
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New Construction Post',
			'edit_item'          => 'Edit Construction Post',
			'new_item'           => 'New Construction Post',
			'all_items'          => 'All Construction Posts',
			'view_item'          => 'View Construction Post',
			'search_items'       => 'Search Construction Posts',
			'not_found'          => 'No Construction Posts Found',
			'not_found_in_trash' => 'No Construction Posts Found In Trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Construction',
		);
		
		$args = array (
			'labels'        => $labels,
			'public'        => true,
			'rewrite'       => array( 'slug' => 'Construction' ),
		);

		register_post_type ( 's_construction', $args );		
	}			
	
	add_action( 'init', 'construction_custom_post' );	

	// flush for permalinks
	function my_rewrite_flush_construction() {
		flush_rewrite_rules();
	}
	add_action( 'after_switch_theme', 'my_rewrite_flush_construction' );
?>