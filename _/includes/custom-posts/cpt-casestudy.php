<?php

/*
	*	-----------------------------------------------------------------
	*	This file creates and contains the casestudy post type
	*	-----------------------------------------------------------------
*/

	// Create s_case_study custom post type
	
	function case_study_custom_post() {
		
		$labels = array (
			'name'               => 'Case Study',
			'singular_name'      => 'Case Study',
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New Case Study Post',
			'edit_item'          => 'Edit Case Study Post',
			'new_item'           => 'New Case Study Post',
			'all_items'          => 'All Case Study Posts',
			'view_item'          => 'View Case Study Post',
			'search_items'       => 'Search Case Study Posts',
			'not_found'          => 'No Case Study Posts Found',
			'not_found_in_trash' => 'No Case Study Posts Found In Trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Case Study',
		);
		
		$args = array (
			'labels'        => $labels,
			'public'        => true,
			'rewrite'       => array( 'slug' => 'Case-Study' ),
		);

		register_post_type ( 's_case_study', $args );		
	}			
	
	add_action( 'init', 'case_study_custom_post' );	

	// flush for permalinks
	function my_rewrite_flush_case_study() {
		flush_rewrite_rules();
	}
	add_action( 'after_switch_theme', 'my_rewrite_flush_case_study' );
?>