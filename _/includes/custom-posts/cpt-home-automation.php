<?php

/*
	*	-----------------------------------------------------------------
	*	This file creates and contains the home automation post type
	*	-----------------------------------------------------------------
*/

	// Create s_home_automation custom post type
	
	function home_automation_custom_post() {
		
		$labels = array (
			'name'               => 'Home Automation',
			'singular_name'      => 'Home Automation',
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New Home Automation Post',
			'edit_item'          => 'Edit Home Automation Post',
			'new_item'           => 'New Home Automation Post',
			'all_items'          => 'All Home Automation Posts',
			'view_item'          => 'View Home Automation Post',
			'search_items'       => 'Search Home Automation Posts',
			'not_found'          => 'No Home Automation Posts Found',
			'not_found_in_trash' => 'No Home Automation Posts Found In Trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Home Automation',
		);
		
		$args = array (
			'labels'        => $labels,
			'public'        => true,
			'rewrite'       => array( 'slug' => 'Home-Automation' ),
//			'supports'       => array( 'title', 'editor', 'excerpt' ),
//			'menu_position' => 20,	
		);

		register_post_type ( 's_home_automation', $args );		
	}			
	
	add_action( 'init', 'home_automation_custom_post' );	

	// flush for permalinks
	function my_rewrite_flush_home_automation() {
		flush_rewrite_rules();
	}
	add_action( 'after_switch_theme', 'my_rewrite_flush_home_automation' );
?>