<?php 

/**
 * Template Name: Construction
 *
 */

get_header();
	
	$args = array(
		'post_type'      => 's_construction',
		'posts_per_page' => -1,
		//'orderby'        => 'menu_order',
		//'order'          => 'ASC',
	);

	$home_automation_items = new WP_Query( $args );

?>
	
	<div class="construction-page">
				
		<?php
			
			$header_image = get_field('header_image');
			$header_image = $header_image[sizes][header_bgd];
			$header_title = get_field('header_title');
			$header_tag   = get_field('header_tag');
		
		?>
		<div class="page-header" style="background-image: url(<?php echo $header_image; ?>); ">
			
			<?php
			
			if ($header_title) : echo '<h1>', $header_title, '</h1>'; endif;
			if ($header_tag) : echo $header_tag; endif;	
			
			?>
		
		</div><!-- /.page-header ends -->		
		
		
		<div class="page-opening">
			<div class="inner-text">
		
			<?php 
				
				$opening_title = get_field('opening_title');
				$opening_text  = get_field('opening_text');

				if ($opening_title) : echo '<h2>', $opening_title, '</h2>'; endif;
				if ($opening_text) : echo $opening_text; endif;
			
			?>
						
			</div><!-- /.inner-text ends -->
		</div><!-- /.page-opening ends -->
		
		<?php if($home_automation_items->have_posts()) : ?>
		
			<?php $count = 0; ?>
			
			<div class="services-wrapper">
				
			<?php while ($home_automation_items->have_posts()) : $home_automation_items->the_post() ?>
				
				<?php $count++; ?>
				
				<div class="page-split">
					
					<div class="first sibling">
					
						<div class="split-text">
							<?php $count = ($count < 10) ? 0 . $count : $count; ?>
							<p class="split-text-number"><?php echo $count; ?></p>
							
							<?php
							
								$item_title   = get_field('header_title');
								$item_image   = get_field('header_image');
								$item_image   = $item_image[sizes][page_split];
								$excerpt_text = get_field('excerpt_text');
								
								if ($item_title) : echo '<h3>', $item_title, '</h3>'; endif;
								if ($excerpt_text) : echo $excerpt_text; endif;

							?>
							<a href="<?php the_permalink(); ?>" class="button">find out more</a>
						</div><!--/.split-text ends -->
				
					</div><!--/.first ends -->
					
					<div class="second makeSiblingHeight responsive-big" style="background-image: url(<?php echo $item_image; ?>);"></div><!--/.second ends -->
					
					<div class="responsive-small">
						<img src="<?php echo $item_image; ?>"/>
					</div>
					
				</div><!--/.page-split ends -->
				
				<?php endwhile; ?>
			
			</div><!-- /.services-wrapper ends -->
		
		<?php endif; ?>
		
		<?php wp_reset_postdata(); ?>
		
	</div><!--/.construction-page ends -->
		
<?php get_footer(); ?>