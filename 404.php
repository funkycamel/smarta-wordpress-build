<?php include 'header.php'; ?>
	
	<!-- / temp class that will be generated dynamically and added to body class in wordpress -->	
	<div class="page-404">
		
		<div class="page-header">
			<h1>404</h1>
			<p class="tag">Apologies, but the page you're looking for could not be found.</p>
			<p>Our award winning team at Smarta Technology offer customers high quality and 
affordable Home Automation installations to satisfy any given specification.</p>
		</div><!-- /.page-header ends -->		
					
	</div><!-- /.page-404 ends -->
		
<?php include 'footer.php'; ?>