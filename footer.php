		<footer class="main-footer">

		<?php
			
			$about_us = get_field('about_us_section');

			if($about_us) :
			
				$about_us_title = get_field('about_us_title');
				$about_us_text  = get_field('about_us_text');
				
				$about_us_first_split_title             = get_field('about_us_first_split_title');
				$about_us_first_split_text              = get_field('about_us_first_split_text');
				$about_us_first_split_background_image  = get_field('about_us_first_split_background_image');
				$about_us_first_split_link              = get_field('about_us_first_split_link');
				
				$about_us_second_split_title            = get_field('about_us_second_split_title');
				$about_us_second_split_text             = get_field('about_us_second_split_text');
				$about_us_second_split_background_image = get_field('about_us_second_split_background_image');
				$about_us_second_split_link             = get_field('about_us_second_split_link');
								
			?>
		
				<div class="about-us-section">
					
					<div class="inner-text">
						<?php if($about_us_title) : echo '<h2>', $about_us_title, '</h2>'; endif; ?>
						<?php if ($about_us_text) : echo $about_us_text; endif; ?>
					</div><!-- /.inner-text ends -->
					
					<div class="about-us-split">
						
						<div class="first" style="background-image: url(<?php echo $about_us_first_split_background_image; ?>)">
							<div class="text">
								<?php if($about_us_first_split_title) : echo '<h4>', $about_us_first_split_title, '</h4>'; endif; ?>
								<?php if($about_us_first_split_text) : echo $about_us_first_split_text; endif; ?>
								<a class="button" href="<?php echo $about_us_first_split_link; ?>">read more</a>
							</div><!-- /.text ends -->
						</div><!-- /.first ends -->
						
						<div class="second" style="background-image: url(<?php echo $about_us_second_split_background_image; ?>)">
							<div class="text">
								<?php if($about_us_first_split_title) : echo '<h4>', $about_us_second_split_title, '</h4>'; endif; ?>
								<?php if($about_us_first_split_text) : echo $about_us_second_split_text; endif; ?>
								<a class="button" href="<?php echo $about_us_second_split_link; ?>">read more</a>
							</div><!-- /.text ends -->
						</div><!-- /.second ends -->
						
					</div><!-- /.about-us-split ends -->
				
				</div><!-- /.about-us-section ends -->
		
			<?php endif; ?>
			
			<div class="upper-footer">
				<div class="inner-text">
					<h5>Like what you see? Give us a call!</h5>
					<p>If you would like to discuss your home automation or construction project in more detail please get in contact with us today!</p>
					<div class="link-wrapper">
						<a class="button" href="tel:02920006588">02920 006 588</a>
						<a class="button" href="<?php echo esc_url( home_url( '/contact' ) ); ?>">message us</a>
					</div>
				</div><!-- /.inner-text ends -->
				
			</div><!-- /.upper-footer ends -->


			
			<div class="lower-footer">
				
				<div class="footer-inner">
				
					<div class="first-section">
						
						<div class="menu-section">
							<h6>home automation</h6>	
							<?php 
								wp_nav_menu( array( 
									'theme_location' => 'home_automation_footer',
									'container' => 'nav',
									'container_class' => 'footer-nav'
									) 
								); 
							?>		
						</div><!-- /.menu-section ends -->
		
						<div class="menu-section">
							<h6>construction</h6>	
							<?php 
								wp_nav_menu( array( 
									'theme_location' => 'construction_footer',
									'container' => 'nav',
									'container_class' => 'footer-nav'
									) 
								); 
							?>	
						</div><!-- /.menu-section ends -->
		
						<div class="menu-section">
							<h6>about</h6>
							<?php 
								wp_nav_menu( array( 
									'theme_location' => 'about_footer',
									'container' => 'nav',
									'container_class' => 'footer-nav'
									) 
								); 
							?>
						</div><!-- /.menu-section ends -->
							
					</div><!-- /.first-section ends -->
					
					<div class="second-section">
						
						<div class="upper">
							
							<h6>follow us</h6>
							
							<ul class="icons">
								<li><a href="https://twitter.com/SmartaTech" target="_blank"><img src="<?php jp_image('twitter.png'); ?>"/></a></li>
								<li><a href="https://www.facebook.com/SmartaTechnology/" target="_blank"><img src="<?php jp_image('facebook.png'); ?>"/></a></li>
								<li><a href="https://plus.google.com/113664526048011640424/about" target="_blank"><img src="<?php jp_image('google.png'); ?>"/></a></li>
<!-- 								<li><a href="#"><img src="<?php jp_image('instagram.png'); ?>"/></a></li> -->
							</ul>
							
						</div><!-- /.upper ends -->
						
						<div class="lower">
							
							<h6>Smarta Construction & Technology</h6>
							
							<p>Unit 2</p>
							<p>Pacific Business Park</p>
							<p>Cardiff</p>
							<p>CF24 5HJ</p>
							<p class="tel"><a href="tel:02920006588">02920 006588</a></p>
							<p class="email"><a href="mailto:enquiries@thinksmarta.co.uk?Subject=Enquiry%20from%20smarta%20website">enquiries@thinksmarta.co.uk</a></p>
						</div><!-- /.lower ends -->
						
					</div><!-- /.second-section ends -->
				
				</div><!-- /.footer-inner ends -->
				
				<div class="copyright-section">
					<div class="footer-inner">
						<p>Copyright &copy; <?php echo date("Y") ?> Smarta Construction & Technology Ltd</p>
					</div><!--/. footer-inner ends -->
				</div><!-- /.copyright-section ends -->
			
			</div><!-- /.lower-footer ends -->
			
		</footer><!-- /.main-footer ends -->
	
	</div><!-- /.content-wrapper ends -->
	
</div><!-- /.main-wrapper ends -->


<?php wp_footer(); ?>
</body>
</html>