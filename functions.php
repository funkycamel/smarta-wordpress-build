<?php
/*-----------------------------------------------------------------------------------*/
/*	Set up constants
/*-----------------------------------------------------------------------------------*/

	define ("FOLDER", get_template_directory_uri());
	define ("IMAGES", FOLDER . "/_/images");

/*-----------------------------------------------------------------------------------*/
/*	Setup
/*-----------------------------------------------------------------------------------*/

	function smarta_setup() {
		
		// Enable support for Post Thumbnails
		add_theme_support( 'post-thumbnails' );
		//set_post_thumbnail_size( 980, 999 );
		add_image_size( 'header_bgd', 1440, 510, array('center', 'center') ); // Used throughout
		add_image_size( 'page_split', 720, 720, array('center', 'center') ); // Used throughout
		add_image_size( 'case_study_third', 480, 320, array('center', 'center') ); // Third split used on case study parent page
		add_image_size( 'full_width_split', 1440, 720, array('center', 'center') ); // Full width split used on home automation and construction
		add_image_size( 'testimonial_avatar', 60, 60, array('center', 'center') ); // Used on the testimonial page
	
		// Enable wp_nav_menu() in two locations.
		register_nav_menus( array(
			'main'                   => ( 'The main navigation' ),
			'home_automation_footer' => ( 'The home automation headed menu located in the footer' ),
			'construction_footer'    => ( 'The construction headed menu located in the footer' ),
			'about_footer'           => ( 'The about headed menu located in the footer' ),
		) );
				
	}
	add_action( 'after_setup_theme', 'smarta_setup' );

/*-----------------------------------------------------------------------------------*/
/*	Include custom posts
/*-----------------------------------------------------------------------------------*/

	include_once( '_/includes/custom-posts/cpt-home-automation.php' ); // Home Automation custom post
	include_once( '_/includes/custom-posts/cpt-construction.php' ); // Construction custom post
	include_once( '_/includes/custom-posts/cpt-casestudy.php' ); // Case Study custom post
	include_once( '_/includes/custom-posts/cpt-testimonial.php' ); // Testimonial custom post

/*-----------------------------------------------------------------------------------*/
/*	Add styles and scripts
/*-----------------------------------------------------------------------------------*/

	function smarta_scripts() {

		wp_register_style( 'bxslider_styles', get_template_directory_uri() . '/_/stylesheets/jquery.bxslider.css');	
		wp_enqueue_style( 'bxslider_styles');
		
		wp_register_style( 'styles', get_template_directory_uri() . '/_/stylesheets/style.css');	
		wp_enqueue_style( 'styles');
	
		wp_register_script( 'images_loaded', get_template_directory_uri() . '/_/js/libs/images-loaded-min.js', '', '', true );
		wp_enqueue_script( 'images_loaded');
		
		wp_register_script( 'isotope', get_template_directory_uri() . '/_/js/libs/isotope-min.js', array('images_loaded'), '', true );
		wp_enqueue_script( 'isotope');
		
		
		wp_register_script( 'bx_slider', get_template_directory_uri() . '/_/js/libs/jquery.bxslider.js', array('jquery'), '', true );
		wp_enqueue_script( 'bx_slider');
		
		wp_register_script('functions', get_template_directory_uri() . '/_/js/functions-min.js', array('jquery', 'bx_slider', 'isotope'), '', true);
		wp_enqueue_script('functions');				
	
	}
	add_action( 'wp_enqueue_scripts', 'smarta_scripts' );


	
/*-----------------------------------------------------------------------------------*/
/*	Google analytics
/*-----------------------------------------------------------------------------------*/


	function smarta_google_analytics() { ?>

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			ga('create', 'UA-50880356-1', 'brightercare.co.uk');
			ga('send', 'pageview');		
		</script>	
	
	<?php }
//	add_action( 'wp_head', 'smarta_google_analytics');


/*-----------------------------------------------------------------------------------*/
/*	Helper functions
/*-----------------------------------------------------------------------------------*/

	// Add direct link to image front end	
	function jp_image($image) {
		echo IMAGES, '/', $image;
	}

	// Helper for testing
	function jp_array($array){
		echo '<pre>', print_r($array), '</pre>';
	}


?>