<!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 

<head>

	<meta charset="utf-8">	
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	
	<title><?php bloginfo( 'name' ); ?> - <?php if ( is_front_page() ) { echo ' Home'; }  wp_title('', true, 'right'); ?></title>	
	<meta name="description" content="<?php bloginfo('description'); ?>" />	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<?php wp_head(); ?>	
</head>

<body>

<div class="main-wrapper">
	
	<header class="main-header">
		
		<div class="menu-btn">
			<div class="menu-btn-middle"></div>
		</div>
				
		<div class="header-inner">
			<a class="header-links" href="<?php echo esc_url( home_url( '/home-automation' ) ); ?>">home automation</a>
			<div class="logo">
				<a href="<?php echo esc_url(home_url()); ?>"></a>
			</div>
			<a class="header-links" href="<?php echo esc_url( home_url( '/construction' ) ); ?>">construction</a>
		</div><!-- /.header-inner ends -->
		
		<a class="tel" href="tel:02920006588"><span>02920 006 588</span></a>
	
	</header><!-- /.main-header ends -->

	<div class="slide-out-navigation">
		
		<div class="nav-header">
			<div class="menu-btn-close">
				<div class="menu-btn-middle"></div>
			</div>
			<p>Browse</p>
			

			<div class="nav-menu-btn">
				<div class="nav-menu-btn-middle"></div>
			</div>
			
		</div>
		
		<?php 
			wp_nav_menu( array( 
				'theme_location' => 'main',
				'container' => 'nav',
				'container_class' => 'main-navigation'
				) 
			); 
		?>	
	
	</div><!-- /.slide-out-navigation ends -->
	
	<div class="content-wrapper">
		<div class="fixed-overlay"></div>
		