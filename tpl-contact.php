<?php 

/**
 * Template Name: Contact
 *
 */

get_header();

?>
	
	<div class="contact-page">
		
		<?php
			
			$header_image = get_field('header_image');
			$header_image = $header_image[sizes][header_bgd];
			$header_title = get_field('header_title');
			$header_tag   = get_field('header_tag');
		
		?>
		<div class="page-header" style="background-image: url(<?php echo $header_image ?>); ">
			
			<?php
			
			if ($header_title) : echo '<h1>', $header_title, '</h1>'; endif;
			if ($header_tag) : echo $header_tag; endif;	
				
			?>
		
		</div><!-- /.page-header ends -->			
		
		<div class="page-split">
			
			<div class="first">
				<?php
					if( function_exists( 'ninja_forms_display_form' ) ) { 
						ninja_forms_display_form( 1 );
					}
				?>
		
			</div><!--/.first ends -->
			
			<div class="second">

				<h2>Contact Details</h2>
				
				<div class="address">
				
					<p class="title">Address</p>
					
					<p>Smarta Construction & Technology</p>
					
					<p>Unit 2</p>
					
					<p>Pacific Business Park</p>
					
					<p>Cardiff</p>
					
					<p>CF24 5HJ</p>
					
				</div><!-- /.address ends -->
				
				<div class="phone">
				
					<p class="title">Phone</p>
				
					<p class="tel"><a href="tel:02920006588">02920 006588</a></p>
					
				</div><!-- /.phone ends -->

				<div class="email">

					<p class="title">email</p>
				
					<p class="email"><a href="mailto:enquiries@thinksmarta.co.uk?Subject=Enquiry%20from%20smarta%20website">enquiries@thinksmarta.co.uk</a></p>
	
				</div><!-- /.email ends -->
			
			</div><!--/.second ends -->
			
		</div><!--/.page-split ends -->
		
	</div><!-- /.contact-page ends -->
		
<?php include 'footer.php'; ?>