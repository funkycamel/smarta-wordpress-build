<?php include 'header.php'; ?>
	
	<div class="casestudy-single-page">

		<?php
			
			$header_image = get_field('header_image');
			$header_image = $header_image[sizes][header_bgd];
			$header_title = get_field('header_title');
			$header_tag   = get_field('header_tag');
		
		?>

		<div class="page-header" style="background-image: url(<?php echo $header_image ?>); ">
		
			<?php
			
				if ($header_title) : echo '<h1>', $header_title, '</h1>'; endif;
				if ($header_tag) : echo $header_tag; endif;	
				
			?>
			
		</div><!-- /.page-header ends -->

		<div class="meta-wrapper">
			
			<?php
				
				$meta_name     = get_field('case_study_meta_name');
				$meta_location = get_field('case_study_meta_location');
				$meta_services = get_field('case_study_meta_services');
			?>
			
			<div class="meta-section">
				<p class="title">Name</p>
				<p class="description"><?php echo $meta_name; ?></p>
			</div><!-- /.meta-section ends -->

			<div class="meta-section">
				<p class="title">Location</p>
				<p class="description"><?php echo $meta_location; ?></p>
			</div><!-- /.meta-section ends -->

			<div class="meta-section">
				<p class="title">Services</p>
				<p class="description"><?php echo $meta_services; ?></p>
			</div><!-- /.meta-section ends -->
			
		</div><!--/. meta-wrapper ends -->

		<div class="slider-section">
			
			<?php $repeater = 'case_study_slider';
				
			if( have_rows($repeater) ) :
			
			while( have_rows($repeater) ) : the_row();
			
				$slider_image   = get_sub_field('case_study_slider_image');
				$slider_title   = get_sub_field('case_study_slider_title');
				$slider_text    = get_sub_field('case_study_slider_text');			
			
			?>
			
			<div class="single-slide">
				
				<img src="<?php echo $slider_image; ?>" alt="case study slider image">
				<div class="slide-meta">
					<p class="slide-heading"><?php echo $slider_title; ?></p>
					<p class="slide-caption"><?php echo $slider_text; ?></p>
				</div>
			</div><!-- /.single-slide ends -->

			<?php endwhile; ?>
			
			<?php endif; ?>
			
		</div><!-- /.slider-section ends -->
		
		
		<div class="page-split">
			
			<?php
			
				$split_image = get_field('case_study_split_image');	
				$split_title = get_field('case_study_split_title');
				$split_text  = get_field('case_study_split_text');
			?>
			
			<div class="first makeSiblingHeight responsive-big" style="background-image: url(<?php echo $split_image; ?>);"></div><!--/.first ends -->
			<div class="responsive-small"><img src="<?php echo $split_image; ?>"/></div>
			
			<div class="second sibling">

				<div class="split-text">
					
					<?php 
						
						if($split_title) : echo '<h3>', $split_title, '</h3>'; endif;
						if($split_text) : echo $split_text; endif;
					
					?>
						
				</div><!--/.split-text ends -->
			
			</div><!--/.second ends -->
			
		</div><!--/.page-split ends -->


		<div class="quote-section">
			<?php
			
				$quote = get_field('case_study_quote');
				$cite  = get_field('case_study_cite');	
			
			?>
			<p class="quote"><?php echo $quote; ?></p>
			<p class="cite"><?php echo $cite; ?></p>
		</div><!-- /.quote-section ends -->

	
						
	</div><!--/.casestudy-single-page ends -->
		
		
<?php include 'footer.php'; ?>