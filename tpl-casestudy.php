<?php 

/**
 * Template Name: Case Study
 *
 */

get_header();
	
	$args = array(
		'post_type'      => 's_case_study',
		'posts_per_page' => -1,
		//'orderby'        => 'menu_order',
		//'order'          => 'ASC',
	);

	$case_study = new WP_Query( $args );

?>
	
	<div class="casestudy-page">

		<?php
			
			$header_image = get_field('header_image');
			$header_image = $header_image[sizes][header_bgd];
			$header_title = get_field('header_title');
			$header_tag   = get_field('header_tag');
		
		?>

		<div class="page-header" style="background-image: url(<?php echo $header_image; ?>); ">

			<?php
			
			if ($header_title) : echo '<h1>', $header_title, '</h1>'; endif;
			if ($header_tag) : echo $header_tag; endif;	
			
			?>

		</div><!-- /.page-header ends -->
		
		<div class="page-opening">
			<div class="inner-text">	

			<?php 
				
				$opening_title = get_field('opening_title');
				$opening_text  = get_field('opening_text');

				if ($opening_title) : echo '<h2>', $opening_title, '</h2>'; endif;
				if ($opening_text) : echo $opening_text; endif;
			
			?>

			</div><!-- /.inner-text ends -->
		</div><!-- /.page-opening ends -->
		
		
		<div class="isotope-section">
			
			<div class="isotope-filters">
				<ul>
					<li><a href="#" data-filter="*" class="selected">All</a></li>
					<li><a href="#" data-filter=".home-automation">Home automation</a></li>
					<li><a href="#" data-filter=".construction">construction</a></li>
				</ul>
			</div><!-- /.isotope-filters ends -->
			
			<div class="isotope-grid">
				
				<?php if($case_study->have_posts()) :
				
					while ($case_study->have_posts()) : $case_study->the_post() ?>

				<?php
					
					$filter_option = get_field('case_study_filter');
					$item_image    = get_field('header_image');
					$item_image    = $item_image[sizes][case_study_third];
					$item_services = get_field('case_study_meta_services');	
				?>
				<div class="isotope-item <?php echo $filter_option; ?>">
					<a href="<?php the_permalink(); ?>">
						<div class="casestudy-image">
							<img src="<?php echo $item_image; ?>" alt="item image"/>
						</div>
						<div class="casestudy-meta">
							<h2><?php the_title(); ?></h2>
							<p><?php echo $item_services; ?></p>
						</div>
					</a>
				</div><!-- /.isotope-item ends -->

				<?php endwhile; ?>
				
				<?php endif; ?>
				
				<?php wp_reset_postdata(); ?>

			</div><!-- /.isotope-grid ends -->	
			
		</div><!-- /.isotope-section ends -->
				
	</div><!--/.casestudy-page ends -->
		
		
<?php include 'footer.php'; ?>